# README #

### What is this repository for? ###

This repository is for a code-a-long where we will go over how to build out a form in PHP.

## Setup ##

### General ###

  * Change site name to "Trudy's Boutique".
  * Change Permalink setting to Post name.

### Contact Page ###

  * Create a page and name it Contact.
  * Change Template to Contact Page template.
  * Add the following code to the Content box.

```
<h2>Let’s Begin Dreaming Today</h2>

Kids love a good mess and a mess is often a sign of a good time,” says award-winning author Donna Erickson. Simple activities like playing in a big pile of leaves or splashing in puddles after an afternoon shower.

<strong>Address:</strong> 12345 Cahaba Heights Ct, Ste 148, Birmingham, Alabama

<strong>Phone Number:</strong> (205) 277-1538
```

  * Publish

### Menu ###

Go to Appearance > Menus and edit the following in the Edit Menus tab

  1. Delete all page links except the Contact link.
  2. Add About, Services, Gallery, Boutique, and Binder in this order above Contact. (NOTE: These can be linked to the home page)
  3. Click the Create Menu button

Then in the Manage Locations tab set the Primary to Menu 1
